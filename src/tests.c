#define _DEFAULT_SOURCE
#define HEAP_SIZE 4096
#include <inttypes.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

inline static void print_test_header(int id) {
    fprintf(stdout, "\n\n-----------[ TEST %d ]-----------\n", id);
}

inline static void print_test_result(int id, int passed) {
    fprintf(stdout, "Test %d %s\n", id, (passed) ? "passed" : "failed");
}

inline static void print_error(char* error) {
    fprintf(stdout, "%s\n", error);
}

inline static void print_error_and_failure_message(int test_id, char* error) {
    print_error(error);
    print_test_result(test_id, 0);
}

inline static void print_test_success(int test_id) {
    print_test_result(test_id, 1);
}

static inline int allocate_one_block(int test_id) {
    print_test_header(test_id);
    void* heap_start = heap_init(1);
    if (heap_start == NULL) {
        print_error_and_failure_message(test_id, "Heap allocation has resulted in null pointer.");
        return 0;
    }

    struct block_header* block = _malloc(HEAP_SIZE / 4);
    debug_heap(stdout, heap_start);

    if (block == NULL) {
        print_error_and_failure_message(test_id, "Memory allocation has resulted in null pointer.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }
    if (block->is_free) {
        print_error_and_failure_message(test_id, "The block returned by memory allocator is free.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }
    if (block->capacity.bytes != HEAP_SIZE / 4) {
        print_error_and_failure_message(test_id, "The block returned by memory allocator doesn't have expected size (HEAP_SIZE / 4).");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }

    _free(block);

    debug_heap(stdout, heap_start);
    if (!block->is_free) {
        print_error_and_failure_message(test_id, "The block hasn't been freed after _free call");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }

    munmap(heap_start, REGION_MIN_SIZE);
    print_test_success(test_id);
    return 1;
}

static inline int free_one_block_of_allocated_memory(int test_id) {
    print_test_header(test_id);
    void *heap_start = heap_init(1);

    if (heap_start == NULL) {
        print_error_and_failure_message(test_id, "Heap allocation has resulted in null pointer.");
        return 0;
    }

    struct block_header *block1 = _malloc(HEAP_SIZE / 8);
    struct block_header *block2 = _malloc(HEAP_SIZE / 8);
    struct block_header *block3 = _malloc(HEAP_SIZE / 8);
    struct block_header *block4 = _malloc(HEAP_SIZE / 8);
    debug_heap(stdout, heap_start);

    _free(block2);

    debug_heap(stdout, heap_start);
    if (!block2->is_free) {
        print_error_and_failure_message(test_id, "Block hasn't been freed.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }
    if (block1->is_free || block3->is_free || block4->is_free) {
        print_error_and_failure_message(test_id, "One or more of the blocks that weren't supposed to be freed have been freed.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }

    munmap(heap_start, REGION_MIN_SIZE);
    print_test_success(test_id);
    return 1;
}

static inline int free_two_blocks_of_allocated_memory(int test_id) {
    print_test_header(test_id);
    void *heap_start = heap_init(1);

    if (heap_start == NULL) {
        print_error_and_failure_message(test_id, "Heap allocation has resulted in null pointer.");
        return 0;
    }

    struct block_header *block1 = _malloc(HEAP_SIZE / 8);
    struct block_header *block2 = _malloc(HEAP_SIZE / 8);
    struct block_header *block3 = _malloc(HEAP_SIZE / 8);
    struct block_header *block4 = _malloc(HEAP_SIZE / 8);
    struct block_header *block5 = _malloc(HEAP_SIZE / 8);
    debug_heap(stdout, heap_start);

    _free(block1);
    _free(block4);

    debug_heap(stdout, heap_start);
    if (!block1->is_free || !block4->is_free) {
        print_error_and_failure_message(test_id, "Block1 or block4 hasn't been freed.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }
    if (block2->is_free || block3->is_free || block5->is_free) {
        print_error_and_failure_message(test_id, "One or more of the blocks that weren't supposed to be freed have been freed.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }

    munmap(heap_start, REGION_MIN_SIZE);
    print_test_success(test_id);
    return 1;
}

static inline int extend_region(int test_id) {
    print_test_header(test_id);
    void* heap_start = heap_init(1);

    if (heap_start == NULL) {
        print_error_and_failure_message(test_id, "Heap allocation has resulted in null pointer.");
        return 0;
    }

    debug_heap(stdout, heap_start);

    struct block_header* result = _malloc(REGION_MIN_SIZE + 1);

    debug_heap(stdout, heap_start);

    if (result != heap_start) {
        print_error_and_failure_message(test_id, "The previous region hasn't been merged with the new one.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }

    if (result->capacity.bytes != REGION_MIN_SIZE + 1) {
        print_error_and_failure_message(test_id, "The capacity of the allocated block is different from expected.");
        munmap(heap_start, REGION_MIN_SIZE);
        return 0;
    }

    munmap(heap_start, REGION_MIN_SIZE);
    print_test_success(test_id);
    return 1;
}

static inline int cant_extend_region_allocate_new_region(int test_id) {
    print_test_header(test_id);
    void* heap = heap_init(1);

    if (heap == NULL) {
        print_error_and_failure_message(test_id, "Heap allocation has resulted in null pointer.");
        return 0;
    }

    void *mapped = mmap((heap + REGION_MIN_SIZE), HEAP_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0);

    if (mapped == MAP_FAILED) {
        print_error_and_failure_message(test_id, "mmap failed.");
        munmap(heap, REGION_MIN_SIZE);
        return 0;
    }

    struct block_header* res = _malloc(REGION_MIN_SIZE + 1);

    debug_heap(stdout, heap);

    if (res == NULL) {
        print_error_and_failure_message(test_id, "Block allocation has resulted in null pointer.");
        munmap(heap, REGION_MIN_SIZE + HEAP_SIZE);
        return 0;
    }

    if (res->capacity.bytes != REGION_MIN_SIZE + 1) {
        print_error_and_failure_message(test_id, "The capacity of the allocated block differs from expected.");
        _free(res);
        munmap(heap, REGION_MIN_SIZE + HEAP_SIZE);
    }

    _free(res);
    munmap(heap, REGION_MIN_SIZE + HEAP_SIZE);
    print_test_success(test_id);
    return 0;
}


void run_tests() {
    int TEST_NUMBER = 5;
    int (*tests[])(int) = {
            [0] = allocate_one_block,
            [1] = free_one_block_of_allocated_memory,
            [2] = free_two_blocks_of_allocated_memory,
            [3] = extend_region,
            [4] = cant_extend_region_allocate_new_region
    };

    int tests_passed = 0;
    for (int i = 0; i < TEST_NUMBER; i++) {
        int test_res = tests[i](i + 1);
        tests_passed += test_res ? 1 : 0;
    }

    printf("%d out of %d tests passed", tests_passed, TEST_NUMBER);
}